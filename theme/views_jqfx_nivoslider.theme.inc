<?php
/**
 * @file
 * Theme & preprocess functions for the Views jQFX: Nivo Slider module.
 */

function template_preprocess_views_jqfx_nivoslider(&$vars) {
  // Initialize our $images array.
  $vars['images'] = array();

  // Strip all images from the $rows created by the original view query.
  foreach ($vars['rows'] as $item) {
    if (preg_match('@(<\s*img\s+[^>]*>)@i', $item, $matches)) {
      $image = $matches[1];
      // If our image is in an anchor tag, use its URL.
      if (preg_match('@<\s*a\s+href\s*=\s*"\s*([^"]+)\s*"[^>]*>[^<]*' . preg_quote($image) . '[^<]*<\s*/a\s*>@i', $item, $urls)) {
        $url = $urls[1];
      }
      // Or look to see if we have any other links in our views items.
      elseif (preg_match('@href\s*=\s*"([^"]+)"@i', $item, $urls)) {
        $url = $urls[1];
      }
      // Otherwise we have no $url.
      else {$url = NULL;}

      // Our image gets wrapped with the $url anchor if it is present.
      if ($url) {
        $image = '<a href="' . $url . '">' . $image . '</a>';
      }

      // Add the image to our image array to display.
      $vars['images'][] = $image;
    }
  }

  $options = $vars['options']['views_jqfx_nivoslider'];

  // Add our slider wrapper for stock or custom themes  
  if ($options['style']) {
    $vars['wrapper'] = 'class="slider-wrapper theme-' . $options['style'] . '"';
  }
  else $vars['wrapper'] = 'class="slider-wrapper theme-custom"';

  _views_jqfx_nivoslider_add_js($options, 'views-jqfx-nivoslider-images-' . $vars['id']);

}

function _views_jqfx_nivoslider_add_js($options, $id) {

  // Find the path to our plugin.
  $path = 'sites/all/libraries';

  // Add the plugin JS and CSS.
  drupal_add_js($path . '/nivo-slider/jquery.nivo.slider.js');
  drupal_add_css($path . '/nivo-slider/nivo-slider.css');

  //Add the module JS
  $drupal_path = drupal_get_path('module', 'views_jqfx_nivoslider');
  drupal_add_js($drupal_path . '/js/views_jqfx_nivoslider.js');

  // Load the theme CSS.
  if ($options['custom_style'] && !$options['style']) {
    $theme_url = check_plain($options['custom_style']);
    drupal_add_css($theme_url);
  }
  elseif ($options['style']) {
    drupal_add_css($path . "/nivo-slider/themes/" . $options['style'] . "/" . $options['style'] . ".css");
  }
  else {$options['style'] = NULL;}

  // Get the javascript settings.
  $settings = array(
    'effect'              => check_plain($options['effect']),
    'slices'              => (int) $options['slices'],
    'boxCols'             => (int) $options['box_cols'],
    'boxRows'             => (int) $options['box_rows'],
    'animSpeed'           => (int) $options['anim_speed'],
    'pauseTime'           => (int) $options['pause_time'],
    'startSlide'          => (int) $options['start_slide'],
    'directionNav'        => (bool) $options['direction_nav'],
    'directionNavHide'    => (bool) $options['direction_nav_hide'],
    'controlNav'          => (bool) $options['control_nav'],
    'keyboardNav'         => (bool) $options['keyboard_nav'],
    'pauseOnHover'        => (bool) $options['pause_on_hover'],
    'manualAdvance'       => (bool) $options['manual_advance'],
    'captionOpacity'      => (float) $options['caption_opacity'],
  );

  // Since controlNav and controlNavThumbs are both boolean we need to translate our 'select' menu options.
  if ($options['control_nav'] == 'thumbnails') {
    $settings['controlNavThumbs'] = TRUE;
    $settings['controlNavThumbsSearch'] = $options['control_nav_thumbs_search'];
    $settings['controlNavThumbsReplace'] = $options['control_nav_thumbs_replace'];
    // Only add the thumbnail css if a custom theme is not chosen
    if (!$options['custom_style']) {
      drupal_add_css($drupal_path . '/theme/thumbnails.css');
    }
  }

  // These will break the Nivo Slider if they are passed empty. Make sure they are set first.
  if ($options['before_change']) {
    $settings['beforeChange'] = $options['before_change'];
  }
  if ($options['after_change']) {
    $settings['afterChange'] = $options['after_change'];
  }
  if ($options['slideshow_end']) {
    $settings['slideshowEnd'] = $options['slideshow_end'];
  }
  if ($options['last_slide']) {
    $settings['lastSlide'] = $options['last_slide'];
  }
  if ($options['after_load']) {
    $settings['afterLoad'] = $options['after_load'];
  }

  // Add the settings array.
  drupal_add_js(array('viewsJqfxNivoslider' => array($id => $settings)), 'setting');
}
