<?php
/**
 * @file
 * The UI and menu includes.
 *
 * Provides the default options array and generates and
 * validates the settings form.
 */

/**
 * Implements hook_views_jqfx_jqfx_types().
 */
function views_jqfx_nivoslider_views_jqfx_jqfx_types() {
  $options = array(
    'views_jqfx_nivoslider' => t('Nivo Slider'),
  );
  return $options;
}

/**
 * Implements hook_views_jqfx_option_definition().
 */
function views_jqfx_nivoslider_views_jqfx_option_definition() {
  // Set our default options.
  $options['views_jqfx_nivoslider'] = array(
    'contains' => array(
      'style'                       => array('default' => 'default'),
      'custom_style'                => array('default' => NULL),
      'effect'                      => array('default' => 'random'),
      'slices'                      => array('default' => '15'),
      'box_cols'                    => array('default' => '8'),
      'box_rows'                    => array('default' => '4'),
      'anim_speed'                  => array('default' => '500'),
      'pause_time'                  => array('default' => '3000'),
      'start_slide'                 => array('default' => '0'),
      'direction_nav'               => array('default' => TRUE),
      'direction_nav_hide'          => array('default' => TRUE),
      'control_nav'                 => array('default' => TRUE),
      'control_nav_thumbs'          => array('default' => TRUE),
      'control_nav_thumbs_search'   => array('default' => 'large'),
      'control_nav_thumbs_replace'  => array('default' => 'thumbnail'),
      'keyboard_nav'                => array('default' => TRUE),
      'pause_on_hover'              => array('default' => TRUE),
      'manual_advance'              => array('default' => FALSE),
      'caption_opacity'             => array('default' => '0.8'),
      'advanced_functions'          => array('default' => FALSE),
      'before_change'               => array('default' => NULL),
      'after_change'                => array('default' => NULL),
      'slideshow_end'               => array('default' => NULL),
      'last_slide'                  => array('default' => NULL),
      'after_load'                  => array('default' => NULL),
    ),
  );
  return $options;
}

/**
 * Implements hook_views_jqfx_views_jqfx_type_form().
 */
function views_jqfx_nivoslider_views_jqfx_jqfx_type_form(&$form, &$form_state, &$view) {
  ctools_include('dependent');
  // Alerts the user if the library is not installed
  if (!file_exists('sites/all/libraries/nivo-slider/jquery.nivo.slider.pack.js')) {
    $form['views_jqfx_nivoslider']['no_nivoslider_js'] = array(
      '#markup' => '<div style="color: red">' . t('You need to download the Nivo Slider plugin and copy it to sites/all/libraries/nivo-slider. You can find the plugin at !url.', array('!url' => l(t('http://nivo.dev7studios.com/', array(), array('langcode' => 'en')), 'http://nivo.dev7studios.com/', array('attributes' => array('target' => '_blank')))), array('langcode' => 'en')) . '</div>',
    );
  }
  $style = array(
    FALSE => t('Custom (Enter the path below)'),
    'default' => t('Default'),
    'orman' => t('Orman'),
    'pascal' => t('Pascal'),
  );
  $form['views_jqfx_nivoslider']['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => t('Default is styled for flexible image sizes and button or optional 50x50 thumbnail navigation. The Orman theme comes styled for a 568x268 image preset and button navigation. The Pascal theme comes styled for a 630x235 image preset and button navigation.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['style'],
    '#options' => $style,
  );
  $form['views_jqfx_nivoslider']['custom_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom theme path'),
    '#description' => t('Enter the path to your custom css file.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['custom_style'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-style' => array(FALSE)),
  );
  $effect = array(
    'random' => t('Random'),
    'sliceDown' => t('Slice down'),
    'sliceDownLeft' => t('Slice down left'),
    'sliceDownRight' => t('Slice down right'),
    'sliceUp' => t('Slice up'),
    'sliceUpLeft' => t('Slice up left'),
    'sliceUpRight' => t('Slice up right'),
    'sliceUpDown' => t('Slice up down'),
    'sliceUpDownLeft' => t('Slice up down left'),
    'fold' => t('Fold'),
    'fade' => t('Fade'),
    'slideInLeft' => t('Slide in left'),
    'slideInRight' => t('Slide in right'),
    'boxRandom' => t('Box random'),
    'boxRain' => t('Box rain'),
    'boxRainReverse' => t('Box rain reverse'),
    'boxRainGrow' => t('Box rain grow'),
    'boxRainGrowReverse' => t('Box rain grow reverse'),
  );
  $form['views_jqfx_nivoslider']['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#description' => t('The transition effect between images'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['effect'],
    '#options' => $effect,
  );
  $form['views_jqfx_nivoslider']['slices'] = array(
    '#type' => 'textfield',
    '#title' => t('Slices'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['slices'],
    '#description' => t('The number of slices used in the transitions.'),
  );
  $form['views_jqfx_nivoslider']['box_cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Box columns'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['box_cols'],
    '#description' => t('The number of columns used in the box style transitions.'),
  );
  $form['views_jqfx_nivoslider']['box_rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Box rows'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['box_rows'],
    '#description' => t('The number of rows used in the box style transitions.'),
  );
  $form['views_jqfx_nivoslider']['anim_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation Speed'),
    '#description' => t('Animation speed in milliseconds'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['anim_speed'],
  );
  $form['views_jqfx_nivoslider']['pause_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Pause Time'),
    '#description' => t('Pause time in milliseconds.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['pause_time'],
  );
  $form['views_jqfx_nivoslider']['start_slide'] = array(
    '#type' => 'textfield',
    '#title' => t('Start slide'),
    '#description' => t('Set starting Slide (0 index).'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['start_slide'],
  );
  $form['views_jqfx_nivoslider']['direction_nav'] = array(
    '#type' => 'select',
    '#title' => t('Direction Navigator'),
    '#description' => t('Previous and next navigation.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['direction_nav'],
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Enabled'),
    ),
  );
  $form['views_jqfx_nivoslider']['direction_nav_hide'] = array(
    '#type' => 'select',
    '#title' => t('Direction Navigator Hide'),
    '#description' => t('Only show direction navigator on mouse over.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['direction_nav_hide'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-direction-nav' => array(TRUE)),
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Enabled'),
    ),
  );
  $form['views_jqfx_nivoslider']['control_nav'] = array(
    '#type' => 'select',
    '#title' => t('Control Navigator'),
    '#description' => t('Chose navigation buttons/thumbs.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['control_nav'],
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Buttons'),
      'thumbnails' => t('Thumbnails'),
    ),
  );
  $presets = array();
  foreach (image_styles() as $p) {
    $presets[$p['name']] = $p['name'];
  }
  $form['views_jqfx_nivoslider']['control_nav_thumbs_search'] = array(
    '#type' => 'select',
    '#title' => t('Slide image style preset'),
    '#description' => t('Select the image style preset that was chosen for the slide images in your view setting.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['control_nav_thumbs_search'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-control-nav' => array('thumbnails')),
    '#options' => $presets,
  );
  $form['views_jqfx_nivoslider']['control_nav_thumbs_replace'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail image style preset'),
    '#options' => $presets,
    '#description' => t('The image style preset to be used for the thumbnail images. The default css is styled for 50x50 images.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['control_nav_thumbs_replace'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-control-nav' => array('thumbnails')),
  );
  $form['views_jqfx_nivoslider']['keyboard_nav'] = array(
    '#type' => 'select',
    '#title' => t('Keyboard Navigator'),
    '#description' => t('Use left and right arrows.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['keyboard_nav'],
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Enabled'),
    ),
  );
  $form['views_jqfx_nivoslider']['pause_on_hover'] = array(
    '#type' => 'select',
    '#title' => t('Pause on Hover'),
    '#description' => t('Stop animation while hovering.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['pause_on_hover'],
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Enabled'),
    ),
  );
  $form['views_jqfx_nivoslider']['manual_advance'] = array(
    '#type' => 'select',
    '#title' => t('Manual Advance'),
    '#description' => t('Force manual transitions.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['manual_advance'],
    '#options' => array(
      FALSE => t('Disabled'),
      TRUE => t('Enabled'),
    ),
  );
  $form['views_jqfx_nivoslider']['caption_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Caption Opacity'),
    '#description' => t('Universal caption opacity.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['caption_opacity'],
  );
  $form['views_jqfx_nivoslider']['advanced_functions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Advanced options'),
    '#description' => t('Check to display the advanced options. These must be entered in the form of a %function. (Caution: experts only!).', array('%function' => 'function(){}')),
    '#default_value' => $view->options['views_jqfx_nivoslider']['advanced_functions'],
  );
  $form['views_jqfx_nivoslider']['before_change'] = array(
    '#type' => 'textarea',
    '#title' => t('Before change'),
    '#description' => t('A function that triggers before a slide change.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['before_change'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-advanced-functions' => array(TRUE)),
  );
  $form['views_jqfx_nivoslider']['after_change'] = array(
    '#type' => 'textarea',
    '#title' => t('After change'),
    '#description' => t('A function that triggers after a slide change.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['after_change'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-advanced-functions' => array(TRUE)),
  );
  $form['views_jqfx_nivoslider']['slideshow_end'] = array(
    '#type' => 'textarea',
    '#title' => t('Slideshow end'),
    '#description' => t('A function that triggers after all slides have been shown.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['slideshow_end'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-advanced-functions' => array(TRUE)),
  );
  $form['views_jqfx_nivoslider']['last_slide'] = array(
    '#type' => 'textarea',
    '#title' => t('Last slide'),
    '#description' => t('A function that triggers when last slide is shown.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['last_slide'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-advanced-functions' => array(TRUE)),
  );
  $form['views_jqfx_nivoslider']['after_load'] = array(
    '#type' => 'textarea',
    '#title' => t('After load'),
    '#description' => t('A function that triggers when slider has loaded.'),
    '#default_value' => $view->options['views_jqfx_nivoslider']['after_load'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-style-options-views-jqfx-nivoslider-advanced-functions' => array(TRUE)),
  );
}

// Validate the form options.
function views_jqfx_nivoslider_views_jqfx_options_form_validate(&$form, &$form_state, &$view) {
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['slices'])) {
    form_error($form['views_jqfx_nivoslider']['slices'], t('!setting must be numeric!', array('Slices')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['box_cols'])) {
    form_error($form['views_jqfx_nivoslider']['box_cols'], t('!setting must be numeric!', array('Box columns')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['box_rows'])) {
    form_error($form['views_jqfx_nivoslider']['box_rows'], t('!setting must be numeric!', array('Box rows')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['anim_speed'])) {
    form_error($form['views_jqfx_nivoslider']['anim_speed'], t('!setting must be numeric!', array('Animation speed')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['pause_time'])) {
    form_error($form['views_jqfx_nivoslider']['pause_time'], t('!setting must be numeric!', array('Pause time')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['start_slide'])) {
    form_error($form['views_jqfx_nivoslider']['start_slide'], t('!setting must be numeric!', array('Start slide')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_jqfx_nivoslider']['caption_opacity'])) {
    form_error($form['views_jqfx_nivoslider']['caption_opacity'], t('!setting must be numeric!', array('Universal caption opacity')));
  }
}
